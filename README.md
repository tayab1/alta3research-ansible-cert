This playbook can Register,Delete or Update host Variables from a database.
API call will be in the script and we as users pass what actions we want to send to DB weather to Register,Delete or update the host in DB 
To Register without Providing Host Variables use  -e "input=Register provided=No"
To register with users input use -e "input=Register provided=Yes users_input={Host vars in json}"
To Delete use -e "input=Delete"
To update Host Variables -e "input=Update provided=Yes user_input={Host vars in json}"
